package core;

import gui.LoginForm;

import java.sql.SQLException;

import javax.swing.UIManager;

import database.DatabaseAdapter;

/*
 * This class will execute everything for the application.
 */
public class Environment {
	public static DatabaseAdapter currentDatabase;

	public static void main(String[] args) {
		
		// Try to set theme
		try {
			// Set theme
			UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
		}

		// Uh-oh, something went wrong, cannot locate package
		catch (Exception ex) {
			// Error getting Theme, Set a default theme here?
		}
	
		// Call the Database
		currentDatabase = new DatabaseAdapter();

		// Attempt to make connection with database
		try {
			currentDatabase.OpenConnection();

		}

		// Database Driver was not found
		catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Database was not found
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Call the GUI
		LoginForm gui = new LoginForm();

		// Run the GUI
		gui.Execute();

	}
}
