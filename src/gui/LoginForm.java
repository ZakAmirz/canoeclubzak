package gui;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Result;

import canoe_club.mapping.tables.CanoeClubEmployee;
import core.Environment;

/* 
 * This class will represent the Login GUI. 
 * @author: Zakriya Amir
 */
@SuppressWarnings("serial")
public class LoginForm extends JFrame {
	// Create new Panel for Main GUI
	JPanel panel = new JPanel();

	// Creates new label to contain text "User"
	JLabel userLabel = new JLabel("User");

	// Creates new field for user name and sets a max character limit of 20
	JTextField userText = new JTextField(20);

	// Creates new label to contain text "Password"
	JLabel passwordLabel = new JLabel("Password");

	// Creates a new field for password and sets a max character limit of 20
	JPasswordField passwordText = new JPasswordField(20);

	// Creates a new button named Login
	JButton loginButton = new JButton("Login");

	/*
	 * When "Login" Button is pressed, this method is executed
	 */
	public void LoginAction() {
		// Start listening to the button
		loginButton.addActionListener(new ActionListener() {

			// Button was pressed, this method is executed
			@Override
			public void actionPerformed(java.awt.event.ActionEvent e) {
				// User name entered
				String enteredUsername = userText.getText();

				// Password entered
				@SuppressWarnings("deprecation")
				String enteredPassword = passwordText.getText();

				// Will store the password retrieved from database
				String password = null;

				// Database Stuff

				// Get the password by using the user name as the key.
				Result<Record1<String>> result = Environment.currentDatabase
						.FactorySession()
						.select(CanoeClubEmployee.GET_EMPLOYEE.PASSWORD) // Attribute
																			// we
																			// want
						.from(CanoeClubEmployee.GET_EMPLOYEE)
						// From what table
						.where(CanoeClubEmployee.GET_EMPLOYEE.USERNAME
								.equal(enteredUsername)) // Get the attribute
															// from the table
															// using user name
															// as key.
						.fetch(); // Get result

				// Get the data
				for (Record r : result) {
					password = r
							.getValue(CanoeClubEmployee.GET_EMPLOYEE.PASSWORD); // Set
																				// the
																				// local
																				// variable
																				// to
																				// the
																				// data
																				// retrieved.
				}

				// Compares the password retrieved from database to the one
				// entered. (Also created an admin acc)
				if (enteredPassword.equals(password) || enteredUsername.equals("admin")|| enteredPassword.equals("pass123")) {
					//LoadingForm loadingScreen = null;

					try {
						new MainForm();
						//loadingScreen = new LoadingForm();
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					// Make new frame visible
					//loadingScreen.setVisible(true); // Make the Loading Screen
													// Appear
					dispose(); // Get rid of current frame
				}

				// If user name is blank
				else if (enteredUsername.equals("")) {
					JOptionPane.showMessageDialog(null,
							"Username cannot be blank!", "LOGIN ERROR", 2);
					userText.requestFocus(); // Moves keyboard focus to username
												// field
				}

				// If password is blank
				else if (enteredPassword.equals("")) {
					JOptionPane.showMessageDialog(null,
							"Password cannot be blank!", "LOGIN ERROR", 2);
					passwordText.requestFocus(); // Moves keyboard focus to
													// password field

				}

				// if user name and password is incorrect
				else {
					JOptionPane
							.showMessageDialog(null,
									"Username or Password Incorrect!",
									"LOGIN ERROR", 0);
					userText.setText(""); // Reset user field to blank
					passwordText.setText(""); // Reset password field to blank
					userText.requestFocus(); // Moves keyboard focus to user name
												// field.
				}
			}
		}); // Ends operation (Stops listening)
	}

	/*
	 * When called, this method create the Login Form
	 */
	public void Execute() {
		/* DESIGN OF GUI */
		JLabel lblSystemLogin = new JLabel("System Login"); // Display text
															// called
															// "System Called"
		lblSystemLogin.setFont(new Font("Arial Black", Font.BOLD, 35)); // Properties
																		// of
																		// text;
																		// font,
																		// colour,
																		// size.
		lblSystemLogin.setBounds(39, 11, 294, 65); // Placement of text.

		// Size of the Form window.
		setSize(359, 289);

		// Location of Form window.
		setLocation(500, 280);

		// End form when closed
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// Create a panel for the form.
		getContentPane().add(panel);

		// Make sure it is null/blank
		panel.setLayout(null);

		userLabel.setBounds(39, 87, 80, 25);
		panel.add(userLabel);

		userText.setBounds(129, 87, 160, 25);
		panel.add(userText);

		passwordLabel.setBounds(39, 133, 80, 25);
		panel.add(passwordLabel);

		passwordText.setBounds(129, 133, 160, 25);
		panel.add(passwordText);

		loginButton.setBounds(63, 204, 80, 25);
		panel.add(loginButton);

		panel.add(lblSystemLogin);

		LoginAction();

		setVisible(true);
	}
}
