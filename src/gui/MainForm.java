package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.jooq.Record;
import org.jooq.Result;

import canoe_club.mapping.tables.CanoeClubEmployee;
import canoe_club.mapping.tables.CanoeClubMember;
import core.Environment;

/* 
 * This class will represent the Main GUI. 
 * @author: Zakriya Amir
 */
@SuppressWarnings("serial")
public class MainForm extends JFrame {

	private JPanel contentPane;

	/* Member Editor Variables */

	// The ID that the user will input
	private JTextField insertMemberId;

	// Will be used to display the Members Id.
	private JTextField memberIDTextField;

	// This integer will hold the parsed value of InsertMemberId
	private int retrievedId = 0;

	// Will store the Members first name
	private JTextField memberFirstName;

	// Will store the Members last name
	private JTextField memberLastName;

	// Will store the Members address
	private JTextField memberAddress;

	// Will store the Members date of birth
	private JTextField memberDateOfBirth;

	// SQL Date Class, will be used to converted Date (Java.util) to Date (Sql)
	private Date memberSQLDateOfBirth = null;

	// The format that the dates will be displayed in
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	// Will store the Members postcode
	private JTextField memberPostCode;

	// Will store the status of Members
	private JTextField memberActiveStatus;

	// Boolean will confirm if the Member has been loaded into the GUI.
	private boolean memberLoaded = false;

	/* Employee GUI Variables */
	private JTextField insertEmployeeID;
	private int employeeRetrievedId = 0;
	private JTextField employeeIDTextField;
	private JTextField employeeFirstName;
	private JTextField employeeLastName;
	private JTextField employeeDateOfBirth;
	// SQL Date Class, will be used to converted Date (Java.util) to Date (Sql)
	private Date employeeSQLDateOfBirth = null;
	private Date employeeSQLStartDate = null;

	private JTextField employeeStartDate;
	private JTextField employeeAddress;
	private JTextField employeePostcode;
	private JTextField employeeStatus;
	private JTextField employeeJobTitle;
	private JTextField employeeShiftPattern;
	private JTextField employeeHourlyPay;
	private boolean employeeLoaded = false;

	/* Table Variables */

	// Table in GUi that will display Member Information
	private JTable membersTable;

	// Table in GUI that will display Employee Information
	private JTable employeesTable;

	// Will store all data of members into vector.
	private Vector<Vector<String>> memberData = new Vector<Vector<String>>();

	// Will store all data of employee into vector
	private Vector<Vector<String>> employeeData = new Vector<Vector<String>>();

	/* Button Variables */

	// Load Member Button
	private JButton btnLoadMember;

	// Add Member Button
	private JButton btnAddMember;

	// Unload Member Button
	private JButton btnUnloadMember;

	// Update Member Button
	private JButton btnUpdateMember;

	// Delete Member Button
	private JButton btnDeleteMember;

	// Load Employee Button
	private JButton btnLoadEmployee;

	// Unload Employee Button
	private JButton unloadButton;

	// Add Employee Button
	private JButton btnAddEmployee;

	// Update Employee Button
	private JButton btnUpdateEmployee;

	// Delete Employee Button
	private JButton btnDeleteEmployee;

	/**
	 * Create the frame.
	 */
	public MainForm() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1362, 637);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		contentPane.add(tabbedPane, BorderLayout.CENTER);

		/* Members Table Created Here */
		JPanel panel_4 = new JPanel();
		tabbedPane.addTab("Members", null, panel_4, null);
		panel_4.setLayout(null);

		// Create the members table
		membersTable = new JTable(memberModel());
		membersTable.setRowSelectionAllowed(false);
		membersTable.setBounds(1, 1, 958, 413);
		membersTable.setPreferredSize(new Dimension(823, 400));

		JScrollPane memberScrollPane = new JScrollPane(membersTable);
		memberScrollPane
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		memberScrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		memberScrollPane.setBounds(0, 5, 1317, 402);

		panel_4.add(memberScrollPane);

		/* Employees Table Created Here */

		JPanel panel_5 = new JPanel();
		tabbedPane.addTab("Employees", null, panel_5, null);
		panel_5.setLayout(null);

		// Load model into table
		employeesTable = new JTable(employeeModel());
		employeesTable.setRowSelectionAllowed(false);
		employeesTable.setBounds(1, 1, 958, 413);
		employeesTable.setPreferredSize(new Dimension(823, 400));

		JScrollPane employeesScrollPane = new JScrollPane(employeesTable);
		employeesScrollPane
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		employeesScrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		employeesScrollPane.setBounds(0, 5, 1317, 402);

		panel_5.add(employeesScrollPane);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Member Editor", null, panel, null);
		panel.setLayout(null);

		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setBackground(Color.WHITE);
		lblFirstName.setForeground(Color.WHITE);
		lblFirstName.setBounds(77, 99, 91, 22);
		panel.add(lblFirstName);

		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setForeground(Color.WHITE);
		lblLastName.setBackground(Color.WHITE);
		lblLastName.setBounds(77, 134, 91, 22);
		panel.add(lblLastName);

		JLabel lblDateOfBirth = new JLabel("Date of Birth:");
		lblDateOfBirth.setForeground(Color.WHITE);
		lblDateOfBirth.setBackground(Color.WHITE);
		lblDateOfBirth.setBounds(77, 169, 91, 22);
		panel.add(lblDateOfBirth);

		JLabel lblAddress = new JLabel("Address:");
		lblAddress.setForeground(Color.WHITE);
		lblAddress.setBackground(Color.WHITE);
		lblAddress.setBounds(77, 204, 91, 22);
		panel.add(lblAddress);

		JLabel lblPostcode = new JLabel("Postcode:");
		lblPostcode.setForeground(Color.WHITE);
		lblPostcode.setBackground(Color.WHITE);
		lblPostcode.setBounds(77, 239, 91, 22);
		panel.add(lblPostcode);

		JLabel lblMembership = new JLabel("Membership:");
		lblMembership.setForeground(Color.WHITE);
		lblMembership.setBackground(Color.WHITE);
		lblMembership.setBounds(77, 274, 91, 22);
		panel.add(lblMembership);

		memberFirstName = new JTextField();
		memberFirstName.setBounds(180, 99, 116, 22);
		panel.add(memberFirstName);
		memberFirstName.setColumns(10);

		memberLastName = new JTextField();
		memberLastName.setBounds(180, 134, 116, 22);
		panel.add(memberLastName);
		memberLastName.setColumns(10);

		memberAddress = new JTextField();
		memberAddress.setBounds(180, 204, 116, 22);
		panel.add(memberAddress);
		memberAddress.setColumns(10);

		memberPostCode = new JTextField();
		memberPostCode.setBounds(180, 239, 116, 22);
		panel.add(memberPostCode);
		memberPostCode.setColumns(10);

		memberActiveStatus = new JTextField();
		memberActiveStatus.setEditable(false);
		memberActiveStatus.setBounds(180, 274, 116, 22);
		panel.add(memberActiveStatus);
		memberActiveStatus.setColumns(10);

		btnAddMember = new JButton("Add Member");

		addMemberAction();

		btnAddMember.setBounds(77, 375, 116, 25);
		panel.add(btnAddMember);

		btnUpdateMember = new JButton("Update Member");

		updateMemberAction();

		btnUpdateMember.setBounds(205, 375, 141, 25);
		panel.add(btnUpdateMember);

		btnDeleteMember = new JButton("Delete Member");

		deleteMemberAction();

		btnDeleteMember.setBounds(358, 375, 134, 25);
		panel.add(btnDeleteMember);

		JLabel lblMemberId = new JLabel("Member ID:");
		lblMemberId.setForeground(Color.WHITE);
		lblMemberId.setBackground(Color.WHITE);
		lblMemberId.setBounds(77, 64, 91, 22);
		panel.add(lblMemberId);

		memberIDTextField = new JTextField();
		memberIDTextField.setEditable(false);
		memberIDTextField.setColumns(10);
		memberIDTextField.setBounds(180, 64, 116, 22);
		panel.add(memberIDTextField);

		JLabel lblInsertMemberId = new JLabel("Insert Member ID:");
		lblInsertMemberId.setForeground(Color.WHITE);
		lblInsertMemberId.setBackground(Color.WHITE);
		lblInsertMemberId.setBounds(52, 13, 116, 22);
		panel.add(lblInsertMemberId);

		insertMemberId = new JTextField();
		insertMemberId.setColumns(10);
		insertMemberId.setBounds(180, 13, 116, 22);
		panel.add(insertMemberId);

		btnLoadMember = new JButton("Load Member");
		btnLoadMember.setBounds(308, 12, 119, 25);
		panel.add(btnLoadMember);

		btnUnloadMember = new JButton("Unload Member");
		UnloadMemberAction();
		btnUnloadMember.setBounds(452, 12, 134, 25);
		panel.add(btnUnloadMember);

		memberDateOfBirth = new JTextField();
		memberDateOfBirth.setColumns(10);
		memberDateOfBirth.setBounds(180, 169, 116, 22);
		panel.add(memberDateOfBirth);

		JLabel lblFormatYyyymmdd = new JLabel("(Format: yyyy-MM-dd)");
		lblFormatYyyymmdd.setBounds(304, 172, 149, 16);
		panel.add(lblFormatYyyymmdd);

		// Button action
		loadMemberAction();

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Employee Editor", null, panel_1, null);
		panel_1.setLayout(null);

		JLabel lblInsertEmployeeId = new JLabel("Insert Employee ID:");
		lblInsertEmployeeId.setBounds(46, 17, 133, 16);
		lblInsertEmployeeId.setForeground(Color.WHITE);
		lblInsertEmployeeId.setBackground(Color.WHITE);
		panel_1.add(lblInsertEmployeeId);

		insertEmployeeID = new JTextField();
		insertEmployeeID.setBounds(215, 18, 116, 22);
		insertEmployeeID.setColumns(10);
		panel_1.add(insertEmployeeID);

		btnLoadEmployee = new JButton("Load Employee");

		// Listens to Load Employee Button, executed when button is pressed.
		loadEmployee();

		btnLoadEmployee.setBounds(343, 17, 133, 25);
		panel_1.add(btnLoadEmployee);

		JLabel lblEmployeeId = new JLabel("Employee ID:");
		lblEmployeeId.setForeground(Color.WHITE);
		lblEmployeeId.setBackground(Color.WHITE);
		lblEmployeeId.setBounds(81, 60, 91, 22);
		panel_1.add(lblEmployeeId);

		employeeIDTextField = new JTextField();
		employeeIDTextField.setEditable(false);
		employeeIDTextField.setColumns(10);
		employeeIDTextField.setBounds(215, 60, 116, 22);
		panel_1.add(employeeIDTextField);

		JLabel label_2 = new JLabel("First Name:");
		label_2.setForeground(Color.WHITE);
		label_2.setBackground(Color.WHITE);
		label_2.setBounds(81, 95, 91, 22);
		panel_1.add(label_2);

		employeeFirstName = new JTextField();
		employeeFirstName.setColumns(10);
		employeeFirstName.setBounds(215, 95, 116, 22);
		panel_1.add(employeeFirstName);

		JLabel label_3 = new JLabel("Last Name:");
		label_3.setForeground(Color.WHITE);
		label_3.setBackground(Color.WHITE);
		label_3.setBounds(81, 130, 91, 22);
		panel_1.add(label_3);

		employeeLastName = new JTextField();
		employeeLastName.setColumns(10);
		employeeLastName.setBounds(215, 130, 116, 22);
		panel_1.add(employeeLastName);

		JLabel label_4 = new JLabel("Date of Birth:");
		label_4.setForeground(Color.WHITE);
		label_4.setBackground(Color.WHITE);
		label_4.setBounds(81, 165, 91, 22);
		panel_1.add(label_4);

		JLabel label_5 = new JLabel("Address:");
		label_5.setForeground(Color.WHITE);
		label_5.setBackground(Color.WHITE);
		label_5.setBounds(81, 200, 91, 22);
		panel_1.add(label_5);

		employeeAddress = new JTextField();
		employeeAddress.setColumns(10);
		employeeAddress.setBounds(215, 200, 116, 22);
		panel_1.add(employeeAddress);

		JLabel label_6 = new JLabel("Postcode:");
		label_6.setForeground(Color.WHITE);
		label_6.setBackground(Color.WHITE);
		label_6.setBounds(81, 235, 91, 22);
		panel_1.add(label_6);

		employeePostcode = new JTextField();
		employeePostcode.setColumns(10);
		employeePostcode.setBounds(215, 235, 116, 22);
		panel_1.add(employeePostcode);

		JLabel lblEmploymentStatus = new JLabel("Employment Status:");
		lblEmploymentStatus.setForeground(Color.WHITE);
		lblEmploymentStatus.setBackground(Color.WHITE);
		lblEmploymentStatus.setBounds(81, 415, 122, 22);
		panel_1.add(lblEmploymentStatus);

		employeeStatus = new JTextField();
		employeeStatus.setEditable(false);
		employeeStatus.setColumns(10);
		employeeStatus.setBounds(215, 415, 116, 22);
		panel_1.add(employeeStatus);

		JLabel lblHourlyPay = new JLabel("Job Title:");
		lblHourlyPay.setForeground(Color.WHITE);
		lblHourlyPay.setBackground(Color.WHITE);
		lblHourlyPay.setBounds(81, 270, 91, 22);
		panel_1.add(lblHourlyPay);

		employeeJobTitle = new JTextField();
		employeeJobTitle.setColumns(10);
		employeeJobTitle.setBounds(215, 270, 158, 22);
		panel_1.add(employeeJobTitle);

		JLabel lblShiftPattern = new JLabel("Shift Pattern:");
		lblShiftPattern.setForeground(Color.WHITE);
		lblShiftPattern.setBackground(Color.WHITE);
		lblShiftPattern.setBounds(81, 305, 91, 22);
		panel_1.add(lblShiftPattern);

		employeeShiftPattern = new JTextField();
		employeeShiftPattern.setColumns(10);
		employeeShiftPattern.setBounds(215, 305, 116, 22);
		panel_1.add(employeeShiftPattern);

		JLabel lblHourlyPay_1 = new JLabel("Hourly Pay:");
		lblHourlyPay_1.setForeground(Color.WHITE);
		lblHourlyPay_1.setBackground(Color.WHITE);
		lblHourlyPay_1.setBounds(81, 345, 91, 22);
		panel_1.add(lblHourlyPay_1);

		employeeHourlyPay = new JTextField();
		employeeHourlyPay.setColumns(10);
		employeeHourlyPay.setBounds(215, 345, 116, 22);
		panel_1.add(employeeHourlyPay);

		btnAddEmployee = new JButton("Add Employee");

		// Listens to Add Employee Button, if pressed, this method is executed.
		addEmployeeAction();

		btnAddEmployee.setBounds(84, 481, 116, 25);
		panel_1.add(btnAddEmployee);

		btnUpdateEmployee = new JButton("Update Employee");

		// Listens to Update Employee Button, if pressed, this method is
		// executed.
		updateEmployee();

		btnUpdateEmployee.setBounds(212, 481, 141, 25);
		panel_1.add(btnUpdateEmployee);

		btnDeleteEmployee = new JButton("Delete Employee");

		// Listens to Delete Employee Button, if pressed, this method is
		// executed.
		deleteEmployee();

		btnDeleteEmployee.setBounds(365, 481, 134, 25);
		panel_1.add(btnDeleteEmployee);

		JLabel lblStartDate = new JLabel("Start Date:");
		lblStartDate.setForeground(Color.WHITE);
		lblStartDate.setBackground(Color.WHITE);
		lblStartDate.setBounds(81, 380, 91, 22);
		panel_1.add(lblStartDate);

		employeeDateOfBirth = new JTextField();
		employeeDateOfBirth.setColumns(10);
		employeeDateOfBirth.setBounds(215, 165, 116, 22);
		panel_1.add(employeeDateOfBirth);

		JLabel label = new JLabel("(Format: yyyy-MM-dd)");
		label.setBounds(339, 168, 149, 16);
		panel_1.add(label);

		employeeStartDate = new JTextField();
		employeeStartDate.setColumns(10);
		employeeStartDate.setBounds(215, 380, 116, 22);
		panel_1.add(employeeStartDate);

		JLabel label_1 = new JLabel("(Format: yyyy-MM-dd)");
		label_1.setBounds(339, 383, 149, 16);
		panel_1.add(label_1);

		unloadButton = new JButton("Unload Member");

		// Listens to the Unload Button, when pressed this method is executed
		unloadEmployee();

		unloadButton.setBounds(488, 17, 134, 25);
		panel_1.add(unloadButton);

		setVisible(true);
	}

	/* Create the Member Model for JTable */
	public DefaultTableModel memberModel() {
		// Get Member data from database
		Result<Record> memberQuery = Environment.currentDatabase
				.FactorySession().select().from(CanoeClubMember.GET_MEMBER)
				.fetch();

		// Will store the name of the columns from Member database
		Vector<String> memberColumn = new Vector<String>();

		// Name the Columns
		memberColumn.add("MEMBER_ID");
		memberColumn.add("FIRST_NAME");
		memberColumn.add("LAST_NAME");
		memberColumn.add("DATE_OF_BIRTH");
		memberColumn.add("ADDRESS");
		memberColumn.add("POSTCODE");
		memberColumn.add("MEMBER");

		// Get the data for the columns
		for (Record r : memberQuery) {

			// Store the data of the columns into vector.
			Vector<String> columnData = new Vector<String>();

			// Get retrieved data of the columns
			columnData.add(r.getValue(CanoeClubMember.GET_MEMBER.MEMBER_ID)
					.toString());
			columnData.add(r.getValue(CanoeClubMember.GET_MEMBER.FIRST_NAME));
			columnData.add(r.getValue(CanoeClubMember.GET_MEMBER.LAST_NAME));

			Date sqlDate = r.getValue(CanoeClubMember.GET_MEMBER.DATE_OF_BIRTH);

			String dateOfBirth = dateFormat.format(sqlDate);

			columnData.add(dateOfBirth);
			columnData.add(r
					.getValue(CanoeClubMember.GET_MEMBER.FIRST_LINE_ADDRESS));
			columnData.add(r.getValue(CanoeClubMember.GET_MEMBER.POSTCODE));
			columnData.add(r.getValue(CanoeClubMember.GET_MEMBER.ACTIVE_MEMBER)
					.toString());

			// Add data in collection.
			memberData.add(columnData);
		}

		// Generate a model using data and column names.
		DefaultTableModel memberModel = new DefaultTableModel(memberData,
				memberColumn);

		return memberModel;
	}

	/* Create the Employee Model for JTable */
	public DefaultTableModel employeeModel() {
		// Get Employee data from database
		Result<Record> employeeQuery = Environment.currentDatabase
				.FactorySession().select().from(CanoeClubEmployee.GET_EMPLOYEE)
				.fetch();

		// Will store the name of the columns from Member database
		Vector<String> employeeColumn = new Vector<String>();

		// Name the Columns
		employeeColumn.add("EMPLOYEE_ID");
		employeeColumn.add("FIRST_NAME");
		employeeColumn.add("LAST_NAME");
		employeeColumn.add("DATE_OF_BIRTH");
		employeeColumn.add("ADDRESS");
		employeeColumn.add("POSTCODE");
		employeeColumn.add("JOB_TITLE");
		employeeColumn.add("HOURLY_PAY");
		employeeColumn.add("START_DATE");
		employeeColumn.add("SHIFT_PATTERN");
		employeeColumn.add("EMPLOYEE");

		// Get the data for the columns
		for (Record r : employeeQuery) {

			// Store the data of the columns into vector.
			Vector<String> columnData = new Vector<String>();

			// Get retrieved data of the columns
			columnData.add(r.getValue(
					CanoeClubEmployee.GET_EMPLOYEE.EMPLOYEE_ID).toString());
			columnData.add(r
					.getValue(CanoeClubEmployee.GET_EMPLOYEE.FIRST_NAME));
			columnData
					.add(r.getValue(CanoeClubEmployee.GET_EMPLOYEE.LAST_NAME));

			Date sqlDate = r
					.getValue(CanoeClubEmployee.GET_EMPLOYEE.DATE_OF_BIRTH);

			String dateOfBirth = dateFormat.format(sqlDate);

			columnData.add(dateOfBirth);
			columnData
					.add(r.getValue(CanoeClubEmployee.GET_EMPLOYEE.FIRST_LINE_ADDRESS));
			columnData.add(r.getValue(CanoeClubEmployee.GET_EMPLOYEE.POSTCODE));
			columnData
					.add(r.getValue(CanoeClubEmployee.GET_EMPLOYEE.JOB_TITLE));

			// Gets all hourly pay values.
			String hourlyPay = r.getValue(
					CanoeClubEmployee.GET_EMPLOYEE.HOURLY_PAY).toString();

			// Adds � in front of all hourly pay values.
			String payWithPrefix = "�" + hourlyPay;

			// Give modified hourly pay data to collection.
			columnData.add(payWithPrefix);

			columnData.add(r
					.getValue(CanoeClubEmployee.GET_EMPLOYEE.START_DATE)
					.toString());
			columnData.add(r
					.getValue(CanoeClubEmployee.GET_EMPLOYEE.SHIFT_PATTERN));
			columnData.add(r.getValue(
					CanoeClubEmployee.GET_EMPLOYEE.ACTIVE_EMPLOYEE).toString());

			// Add data in collection.
			employeeData.add(columnData);
		}

		// Generate a model using data and column names.
		DefaultTableModel employeeModel = new DefaultTableModel(employeeData,
				employeeColumn);

		return employeeModel;
	}

	/* Finds a member and loads it into the GUI. */
	public void loadMemberAction() {
		btnLoadMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// Return amount of ID's that exist
				int memberIDCount = Environment.currentDatabase
						.FactorySession()
						.fetchCount(CanoeClubMember.GET_MEMBER);

				try {

					// Get the Id from text field and convert to a Int data type
					retrievedId = Integer.parseInt(insertMemberId.getText());

					// If the Id received is 0 then print out a dialog
					if (retrievedId == 0) {
						JOptionPane.showMessageDialog(null, "ID cannot be 0!",
								"ID ERROR", 2);
						insertMemberId.requestFocus();
					}

					// If ID received is bigger than count, then it does not
					// exist within the database
					if (retrievedId > memberIDCount) {
						JOptionPane.showMessageDialog(null,
								"Sorry but this ID does not exist!",
								"ID ERROR", 2);
						insertMemberId.requestFocus();
					}

				}

				// ID was either empty or contained non-integer values
				catch (NumberFormatException e1) {
					JOptionPane
							.showMessageDialog(null,
									"ID must be a number and not empty!",
									"ID ERROR", 2);
					insertMemberId.requestFocus();
				}

				// Get open connection and start the query
				Result<Record> getMemberQuery = Environment.currentDatabase
						.FactorySession().select() // Select
													// All
						// Get data using ID as primary key
						.from(CanoeClubMember.GET_MEMBER)
						.where(CanoeClubMember.GET_MEMBER.MEMBER_ID
								.equal(retrievedId)).fetch(); // Return data
																// found

				// Search through the data that the query has returned.
				for (Record r : getMemberQuery) {

					// Set the ID for the text field
					memberIDTextField.setText(insertMemberId.getText());

					// Set the "First Name" text field from query.
					memberFirstName.setText(r
							.getValue(CanoeClubMember.GET_MEMBER.FIRST_NAME));

					memberLastName.setText(r
							.getValue(CanoeClubMember.GET_MEMBER.LAST_NAME));

					memberDateOfBirth.setText((r
							.getValue(CanoeClubMember.GET_MEMBER.DATE_OF_BIRTH)
							.toString()));

					memberAddress.setText(r
							.getValue(CanoeClubMember.GET_MEMBER.FIRST_LINE_ADDRESS));

					memberPostCode.setText(r
							.getValue(CanoeClubMember.GET_MEMBER.POSTCODE));

					// Instead of displaying True or False, make a string to
					// display Active or Not Active
					String membershipStatus = "Not Active";

					// If true, set membershipStatus as Active
					if (r.getValue(CanoeClubMember.GET_MEMBER.ACTIVE_MEMBER)) {
						membershipStatus = "Active";
					}

					// Set text field to membership status
					memberActiveStatus.setText(membershipStatus);

					memberLoaded = true;
				}
			}
		});
	}

	/* If a member is loaded, this will unload the member from the GUI. */
	public void UnloadMemberAction() {
		btnUnloadMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// If there is a member loaded, then this condition will be met
				if (memberLoaded) {
					memberIDTextField.setText("");

					memberFirstName.setText("");

					memberLastName.setText("");

					memberDateOfBirth.setText("");

					memberAddress.setText("");

					memberPostCode.setText("");

					memberActiveStatus.setText("");

					// Member is now unloaded
					memberLoaded = false;
				}

				else {
					JOptionPane
							.showMessageDialog(
									null,
									"There is no member loaded, please load a member first!",
									"ID ERROR", 2);
				}
			}
		});
	}

	/* Creates a new Member when button is pressed. */
	public void addMemberAction() {
		btnAddMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// When checks are complete, this will be set to true
				boolean makeUser = false;

				// Java Date Class
				java.util.Date dateOfBirth = null;

				// Check if there's a member already loaded
				if (memberLoaded) {
					JOptionPane
							.showMessageDialog(
									null,
									"You cannot add an already existing member! Please Click Unload member.",
									"ID ERROR", 2);
				}

				// Check if any information has not been completed.
				else if (memberFirstName.getText().equals("")
						|| memberLastName.getText().equals("")
						|| memberDateOfBirth.getText().equals("")
						|| memberAddress.getText().equals("")
						|| memberPostCode.getText().equals("")) {
					JOptionPane
							.showMessageDialog(
									null,
									"Oops some information is missing! Make sure you have filled in everything",
									"DATA ERROR", 2);
				}

				// Convert Java Date into SQL Date
				else {

					try {
						dateOfBirth = dateFormat.parse(memberDateOfBirth
								.getText());

						memberSQLDateOfBirth = new Date(dateOfBirth.getTime());

						// Converting date was successful, user is ready to be
						// made.
						makeUser = true;

					}

					// Date format was not correct.
					catch (ParseException e1) {

						// Clear inputed Date
						memberDateOfBirth.setText("");
						memberDateOfBirth.requestFocus();

						// Let user know to enter date correctly.
						JOptionPane.showMessageDialog(null,
								"Incorrect Date format!", "DATE ERROR", 2);

						System.out.println(e1.toString());

						// User is not ready to be made
						makeUser = false;
					}
				}

				// If user is ready to be made, this block of code will run.
				if (makeUser) {
					Environment.currentDatabase
							.FactorySession()
							.insertInto(CanoeClubMember.GET_MEMBER)
							.set(CanoeClubMember.GET_MEMBER.FIRST_NAME,
									memberFirstName.getText())
							.set(CanoeClubMember.GET_MEMBER.LAST_NAME,
									memberLastName.getText())
							.set(CanoeClubMember.GET_MEMBER.DATE_OF_BIRTH,
									memberSQLDateOfBirth)
							.set(CanoeClubMember.GET_MEMBER.FIRST_LINE_ADDRESS,
									memberAddress.getText())
							.set(CanoeClubMember.GET_MEMBER.POSTCODE,
									memberPostCode.getText())
							.set(CanoeClubMember.GET_MEMBER.ACTIVE_MEMBER, true)
							.execute();

					// Let user know that the new member was created
					// successfully.
					JOptionPane.showMessageDialog(null,
							"Member created sucessfully!", "MEMBER CREATED", 3);

					// Deletes Existing data in table
					memberModel().setRowCount(0);

					// Loads new data into table
					memberModel().fireTableDataChanged();
				}
			}

		});
	}

	/* Updates Existing Member Data */
	public void updateMemberAction() {
		btnUpdateMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (memberLoaded) {
					// Check if any information has not been completed.
					if (memberFirstName.getText().equals("")
							|| memberLastName.getText().equals("")
							|| memberDateOfBirth.getText().equals("")
							|| memberAddress.getText().equals("")
							|| memberPostCode.getText().equals("")) {
						JOptionPane
								.showMessageDialog(
										null,
										"Oops some information is missing! Make sure you have filled in everything",
										"DATA ERROR", 2);
					}

					/* Stops the date being null, as we have to convert it! */
					try {

						java.util.Date dateOfBirth = dateFormat
								.parse(memberDateOfBirth.getText());

						memberSQLDateOfBirth = new Date(dateOfBirth.getTime());

						Environment.currentDatabase
								.FactorySession()
								.update(CanoeClubMember.GET_MEMBER)
								.set(CanoeClubMember.GET_MEMBER.FIRST_NAME,
										memberFirstName.getText())
								.set(CanoeClubMember.GET_MEMBER.LAST_NAME,
										memberLastName.getText())
								.set(CanoeClubMember.GET_MEMBER.DATE_OF_BIRTH,
										memberSQLDateOfBirth)
								.set(CanoeClubMember.GET_MEMBER.FIRST_LINE_ADDRESS,
										memberAddress.getText())
								.set(CanoeClubMember.GET_MEMBER.POSTCODE,
										memberPostCode.getText())
								.set(CanoeClubMember.GET_MEMBER.ACTIVE_MEMBER,
										true)
								.where(CanoeClubMember.GET_MEMBER.MEMBER_ID
										.equal(retrievedId)).execute();

						// Deletes Existing data in table
						memberModel().setRowCount(0);

						// Loads new data into table
						memberModel().fireTableDataChanged();

						JOptionPane.showMessageDialog(null, "Member Updated!",
								"MEBER UPDATE", 1);
					}

					catch (Exception ex) {
						System.out.println(ex.getMessage());
					}

				}

				else {
					JOptionPane.showMessageDialog(null,
							"Please load a member first!",
							"UPDATE MEMBER ERROR", 2);
				}
			}
		});
	}

	public void deleteMemberAction() {
		btnDeleteMember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (memberLoaded) {
					int dialogResult = JOptionPane.showConfirmDialog(null,
							"Are you sure you want to delete this memeber?");

					if (dialogResult == JOptionPane.YES_OPTION) {
						Environment.currentDatabase
								.FactorySession()
								.delete(CanoeClubMember.GET_MEMBER)
								.where(CanoeClubMember.GET_MEMBER.MEMBER_ID
										.equal(retrievedId)).execute();

						// Deletes Existing data in table
						memberModel().setRowCount(0);

						// Loads new data into table
						memberModel().fireTableDataChanged();

						memberIDTextField.setText("");

						memberFirstName.setText("");

						memberLastName.setText("");

						memberDateOfBirth.setText("");

						memberAddress.setText("");

						memberPostCode.setText("");

						memberActiveStatus.setText("");

						JOptionPane.showMessageDialog(null,
								"Deletion has been completed!",
								"DELETE MEMBER COMPLETED", 2);

					}

					if (dialogResult == JOptionPane.NO_OPTION) {
						JOptionPane.showMessageDialog(null,
								"Deletion has been cancelled!",
								"DELETE MEMBER CANCELLED", 2);
					}
				}

				else {
					JOptionPane.showMessageDialog(null,
							"Please load a member first!",
							"DELETE MEMBER ERROR", 2);
				}
			}
		});
	}

	public void loadEmployee() {
		btnLoadEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Return amount of ID's that exist
				int employeeIDCount = Environment.currentDatabase
						.FactorySession().fetchCount(
								CanoeClubEmployee.GET_EMPLOYEE);

				try {

					// Get the Id from text field and convert to a Int data type
					employeeRetrievedId = Integer.parseInt(insertEmployeeID
							.getText());

					// If the Id received is 0 then print out a dialog
					if (employeeRetrievedId == 0) {
						JOptionPane.showMessageDialog(null, "ID cannot be 0!",
								"ID ERROR", 2);
						insertEmployeeID.requestFocus();
					}

					// If ID received is bigger than count, then it does not
					// exist within the database
					if (employeeRetrievedId > employeeIDCount) {
						JOptionPane.showMessageDialog(null,
								"Sorry but this ID does not exist!",
								"ID ERROR", 2);
						insertEmployeeID.requestFocus();
					}

				}

				// ID was either empty or contained non-integer values
				catch (NumberFormatException e1) {
					JOptionPane
							.showMessageDialog(null,
									"ID must be a number and not empty!",
									"ID ERROR", 2);
					insertEmployeeID.requestFocus();
				}

				// Get open connection and start the query
				Result<Record> getEmployeeQuery = Environment.currentDatabase
						.FactorySession().select() // Select
													// All
						// Get data using ID as primary key
						.from(CanoeClubEmployee.GET_EMPLOYEE)
						.where(CanoeClubEmployee.GET_EMPLOYEE.EMPLOYEE_ID
								.equal(employeeRetrievedId)).fetch(); // Return
																		// data
																		// found

				// Search through the data that the query has returned.
				for (Record r : getEmployeeQuery) {

					// Set the ID for the text field
					employeeIDTextField.setText(insertEmployeeID.getText());

					// Set the "First Name" text field from query.
					employeeFirstName.setText(r
							.getValue(CanoeClubEmployee.GET_EMPLOYEE.FIRST_NAME));

					employeeLastName.setText(r
							.getValue(CanoeClubEmployee.GET_EMPLOYEE.LAST_NAME));

					employeeDateOfBirth.setText((r
							.getValue(CanoeClubEmployee.GET_EMPLOYEE.DATE_OF_BIRTH)
							.toString()));

					employeeAddress.setText(r
							.getValue(CanoeClubEmployee.GET_EMPLOYEE.FIRST_LINE_ADDRESS));

					employeePostcode.setText(r
							.getValue(CanoeClubEmployee.GET_EMPLOYEE.POSTCODE));

					employeeJobTitle.setText(r
							.getValue(CanoeClubEmployee.GET_EMPLOYEE.JOB_TITLE));

					employeeShiftPattern.setText(r
							.getValue(CanoeClubEmployee.GET_EMPLOYEE.SHIFT_PATTERN));

					String prefixHourlyPay = "�"
							+ r.getValue(
									CanoeClubEmployee.GET_EMPLOYEE.HOURLY_PAY)
									.toString();

					employeeHourlyPay.setText(prefixHourlyPay);

					employeeStartDate.setText(r.getValue(
							CanoeClubEmployee.GET_EMPLOYEE.START_DATE)
							.toString());

					// Instead of displaying True or False, make a string to
					// display Active or Not Active
					String employeeStat = "Not Active";

					// If true, set employeeStatus as Active
					if (r.getValue(CanoeClubEmployee.GET_EMPLOYEE.ACTIVE_EMPLOYEE)) {
						employeeStat = "Active";
					}

					// Set text field to employeeship status
					employeeStatus.setText(employeeStat);

					employeeLoaded = true;
				}
			}
		});
	}

	public void unloadEmployee() {
		unloadButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// If there is a employee loaded, then this condition will be
				// met
				if (employeeLoaded) {
					employeeIDTextField.setText("");

					employeeFirstName.setText("");

					employeeLastName.setText("");

					employeeDateOfBirth.setText("");

					employeeAddress.setText("");

					employeePostcode.setText("");

					employeeJobTitle.setText("");

					employeeShiftPattern.setText("");

					employeeStartDate.setText("");

					employeeHourlyPay.setText("");

					employeeStatus.setText("");

					// employee is now unloaded
					employeeLoaded = false;
				}

				else {
					JOptionPane
							.showMessageDialog(
									null,
									"There is no employee loaded, please load a employee first!",
									"LOAD ERROR", 2);
				}

			}
		});
	}

	public void addEmployeeAction() {
		btnAddEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				// When checks are complete, this will be set to true
				boolean makeEmployee = false;

				// Java Date Class
				java.util.Date dateOfBirth = null;

				java.util.Date startDate = null;

				// Check if there's a employee already loaded
				if (employeeLoaded) {
					JOptionPane
							.showMessageDialog(
									null,
									"You cannot add an already existing employee! Please Click Unload employee.",
									"LOADING ERROR", 2);
				}

				// Check if any information has not been completed.
				else if (employeeFirstName.getText().equals("")
						|| employeeLastName.getText().equals("")
						|| employeeDateOfBirth.getText().equals("")
						|| employeeAddress.getText().equals("")
						|| employeePostcode.getText().equals("")
						|| employeeJobTitle.equals("")
						|| employeeShiftPattern.equals("")
						|| employeeHourlyPay.equals("")
						|| employeeStartDate.equals("")) {
					JOptionPane
							.showMessageDialog(
									null,
									"Oops some information is missing! Make sure you have filled in everything",
									"DATA ERROR", 2);
				}

				// Convert Java Date into SQL Date
				else {

					try {
						dateOfBirth = dateFormat.parse(employeeDateOfBirth
								.getText());

						startDate = dateFormat.parse(employeeStartDate
								.getText());

						employeeSQLDateOfBirth = new Date(dateOfBirth.getTime());

						employeeSQLStartDate = new Date(startDate.getTime());

						// Converting date was successful, employee is ready to
						// be
						// made.
						makeEmployee = true;

					}

					// Date format was not correct.
					catch (ParseException e1) {

						// Clear inputed Date
						employeeDateOfBirth.setText("");
						employeeDateOfBirth.requestFocus();

						// Let user know to enter date correctly.
						JOptionPane.showMessageDialog(null,
								"Incorrect Date format!", "DATE ERROR", 2);

						System.out.println(e1.toString());

						// User is not ready to be made
						makeEmployee = false;
					}
				}

				// If employee is ready to be made, this block of code will run.
				if (makeEmployee) {

					if (employeeHourlyPay.getText().startsWith("�")) {
						JOptionPane.showMessageDialog(null,
								"Remove the � sign from Hourly Pay!",
								"EMPLOYEE ERROR", 3);
					}

					else {
						/* Convert String Hourly Pay to Big Decimal */

						DecimalFormat decimalFormat = new DecimalFormat();

						decimalFormat.setParseBigDecimal(true);

						BigDecimal hourlyPay = null;
						try {
							hourlyPay = (BigDecimal) decimalFormat
									.parse(employeeHourlyPay.getText());
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						// Create new record query
						Environment.currentDatabase
								.FactorySession()
								.insertInto(CanoeClubEmployee.GET_EMPLOYEE)
								.set(CanoeClubEmployee.GET_EMPLOYEE.FIRST_NAME,
										employeeFirstName.getText())
								.set(CanoeClubEmployee.GET_EMPLOYEE.LAST_NAME,
										employeeLastName.getText())
								.set(CanoeClubEmployee.GET_EMPLOYEE.DATE_OF_BIRTH,
										employeeSQLDateOfBirth)
								.set(CanoeClubEmployee.GET_EMPLOYEE.FIRST_LINE_ADDRESS,
										employeeAddress.getText())
								.set(CanoeClubEmployee.GET_EMPLOYEE.POSTCODE,
										employeePostcode.getText())
								.set(CanoeClubEmployee.GET_EMPLOYEE.JOB_TITLE,
										employeeJobTitle.getText())
								.set(CanoeClubEmployee.GET_EMPLOYEE.SHIFT_PATTERN,
										employeeShiftPattern.getText())
								.set(CanoeClubEmployee.GET_EMPLOYEE.START_DATE,
										employeeSQLStartDate)
								.set(CanoeClubEmployee.GET_EMPLOYEE.HOURLY_PAY,
										hourlyPay)
								.set(CanoeClubEmployee.GET_EMPLOYEE.ACTIVE_EMPLOYEE,
										true).execute();

						// Let user know that the new employee was created
						// successfully.
						JOptionPane.showMessageDialog(null,
								"Employee created sucessfully!",
								"EMPLOYEE CREATED", 1);

						// Deletes Existing data in table
						employeeModel().setRowCount(0);

						// Loads new data into table
						employeeModel().fireTableDataChanged();
					}
				}

			}
		});
	}

	public void updateEmployee() {
		btnUpdateEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BigDecimal hourlyPay = null;

				if (employeeLoaded) {
					// Check if any information has not been completed.
					if (employeeFirstName.getText().equals("")
							|| employeeLastName.getText().equals("")
							|| employeeDateOfBirth.getText().equals("")
							|| employeeAddress.getText().equals("")
							|| employeePostcode.getText().equals("")
							|| employeeJobTitle.equals("")
							|| employeeShiftPattern.equals("")
							|| employeeHourlyPay.equals("")
							|| employeeStartDate.equals("")) {
						JOptionPane
								.showMessageDialog(
										null,
										"Oops some information is missing! Make sure you have filled in everything",
										"DATA ERROR", 2);
					}

					/* Stops the date being null, as we have to convert it! */
					try {

						java.util.Date dateOfBirth = dateFormat
								.parse(employeeDateOfBirth.getText());
						java.util.Date startDate = dateFormat
								.parse(employeeStartDate.getText());

						employeeSQLDateOfBirth = new Date(dateOfBirth.getTime());
						employeeSQLStartDate = new Date(startDate.getTime());

					} catch (Exception ex) {
						System.out.println(ex.getMessage());
					}

					if (employeeHourlyPay.getText().startsWith("�")) {
						JOptionPane.showMessageDialog(null,
								"Remove the � sign from Hourly Pay!",
								"EMPLOYEE ERROR", 3);
					}

					else {
						/* Convert String Hourly Pay to Big Decimal */

						DecimalFormat decimalFormat = new DecimalFormat();

						decimalFormat.setParseBigDecimal(true);

						try {
							hourlyPay = (BigDecimal) decimalFormat
									.parse(employeeHourlyPay.getText());
						} catch (ParseException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						Environment.currentDatabase
								.FactorySession()
								.update(CanoeClubEmployee.GET_EMPLOYEE)
								.set(CanoeClubEmployee.GET_EMPLOYEE.FIRST_NAME,
										employeeFirstName.getText())
								.set(CanoeClubEmployee.GET_EMPLOYEE.LAST_NAME,
										employeeLastName.getText())
								.set(CanoeClubEmployee.GET_EMPLOYEE.DATE_OF_BIRTH,
										employeeSQLDateOfBirth)
								.set(CanoeClubEmployee.GET_EMPLOYEE.FIRST_LINE_ADDRESS,
										employeeAddress.getText())
								.set(CanoeClubEmployee.GET_EMPLOYEE.POSTCODE,
										employeePostcode.getText())
								.set(CanoeClubEmployee.GET_EMPLOYEE.JOB_TITLE,
										employeeJobTitle.getText())
								.set(CanoeClubEmployee.GET_EMPLOYEE.SHIFT_PATTERN,
										employeeShiftPattern.getText())
								.set(CanoeClubEmployee.GET_EMPLOYEE.START_DATE,
										employeeSQLStartDate)
								.set(CanoeClubEmployee.GET_EMPLOYEE.HOURLY_PAY,
										hourlyPay)
								.set(CanoeClubEmployee.GET_EMPLOYEE.ACTIVE_EMPLOYEE,
										true)
								.where(CanoeClubEmployee.GET_EMPLOYEE.EMPLOYEE_ID
										.equal(employeeRetrievedId)).execute();

						// Deletes Existing data in table
						employeeModel().setRowCount(0);

						// Loads new data into table
						employeeModel().fireTableDataChanged();

						JOptionPane.showMessageDialog(null,
								"Employee Updated!", "UPDATE EMPLOYEE", 3);

					}
				}

				else {
					JOptionPane.showMessageDialog(null,
							"Please load a employee first!",
							"UPDATE EMPLOYEE ERROR", 2);
				}
			}
		});
	}

	public void deleteEmployee() {
		btnDeleteEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (employeeLoaded) {
					int dialogResult = JOptionPane.showConfirmDialog(null,
							"Are you sure you want to delete this employee?");

					if (dialogResult == JOptionPane.YES_OPTION) {
						Environment.currentDatabase
								.FactorySession()
								.delete(CanoeClubEmployee.GET_EMPLOYEE)
								.where(CanoeClubEmployee.GET_EMPLOYEE.EMPLOYEE_ID
										.equal(employeeRetrievedId)).execute();

						// Deletes Existing data in table
						employeeModel().setRowCount(0);

						// Loads new data into table
						employeeModel().fireTableDataChanged();

						employeeIDTextField.setText("");

						employeeFirstName.setText("");

						employeeLastName.setText("");

						employeeDateOfBirth.setText("");

						employeeAddress.setText("");

						employeePostcode.setText("");

						employeeJobTitle.setText("");

						employeeShiftPattern.setText("");

						employeeStartDate.setText("");

						employeeHourlyPay.setText("");

						employeeStatus.setText("");

						// employee is now unloaded
						employeeLoaded = false;

						JOptionPane.showMessageDialog(null,
								"Deletion has been completed!",
								"DELETE EMPLOYEE COMPLETED", 2);

					}

					if (dialogResult == JOptionPane.NO_OPTION) {
						JOptionPane.showMessageDialog(null,
								"Deletion has been cancelled!",
								"DELETE EMPLOYEE CANCELLED", 2);
					}
				}

				else {
					JOptionPane.showMessageDialog(null,
							"Please load a employee first!",
							"DELETE EMPLOYEE ERROR", 2);
				}
			}
		});
	}
}
