package test;

import junit.framework.TestCase;

import org.jooq.Record1;
import org.jooq.Result;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.notification.Failure;

import canoe_club.mapping.tables.CanoeClubMember;
import core.Environment;

public class DatabaseTest extends TestCase {

	protected int memberId;

	protected String first_name;

	protected boolean activeMember;

	public static void main(String[] args) {
		org.junit.runner.Result result = JUnitCore
				.runClasses(DatabaseTest.class);

		for (Failure failure : result.getFailures()) {
			System.out.println(failure.toString());
		}

		System.out.println(result.wasSuccessful());
	}

	protected void setUp() {
		memberId = 2;
	}

	@Test
	public void testGetUser() {

		// Attempt to get first name
		Result<Record1<String>> nameQuery = Environment.currentDatabase.FactorySession()
				.select(CanoeClubMember.GET_MEMBER.FIRST_NAME)
				.where(CanoeClubMember.GET_MEMBER.MEMBER_ID.equal(memberId))
				.fetch();

		for (Record1<String> r : nameQuery) {
			first_name = r.getValue(CanoeClubMember.GET_MEMBER.FIRST_NAME);
		}
		
		// if first_name is null/empty, then test has failed
		assertNull(first_name);
	}

	public void testActiveMember() {

		// Attempt to get Active Member
		Result<Record1<Boolean>> getActiveMember = Environment.currentDatabase
				.FactorySession()
				.select(CanoeClubMember.GET_MEMBER.ACTIVE_MEMBER)
				.where(CanoeClubMember.GET_MEMBER.MEMBER_ID.equal(memberId))
				.fetch();

		for (Record1<Boolean> r : getActiveMember) {
			activeMember = r.getValue(CanoeClubMember.GET_MEMBER.ACTIVE_MEMBER);
		}

		// Every member that exists is a active member, so if this member
		// isn't "true" active, then test failed.

		assertTrue(activeMember);
	}

}
