package database;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

/*
 * This class creates and manages the database connection.
 * 
 * @author: Zakriya Amir
 */
public class DatabaseAdapter {
	// Declaring the SQL Connection
	private Connection conn = null;

	/*
	 * Attempts to create a new connection. SQLException : Returns any SQL
	 * related errors. ClassNotFoundException: Returns any H2 package related
	 * errors.
	 */
	public void OpenConnection() throws SQLException, ClassNotFoundException {
		// Getting the location of the database
		File f = new File("canoeclub");

		// Loading the H2 Database Package
		Class.forName("org.h2.Driver");

		// Declaring the SQL Connection using JDBC + H2
		this.conn = DriverManager.getConnection("jdbc:h2:" + f.getAbsolutePath(), "admin", "pass123");

		// Debug Console, if connection was successful, this message would be
		// printed out
		System.out.print("Connected to Database sucessfully!");

	}

	/*
	 * Returns the current active connection.
	 */
	public Connection ActiveConnection() {
		return this.conn;
	}

	/*
	 * Returns an active JOOQ Session
	 */
	public DSLContext FactorySession() {
		DSLContext create = DSL.using(this.conn, SQLDialect.H2);

		return create;
	}
}
